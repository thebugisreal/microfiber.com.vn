﻿$(".slider").slick({
	nextArrow: '<button type="button" class="slick-next"><img src="Theme01/Assets/css/images/next.png" alt="next" /></button>',
	prevArrow: '<button type="button" class="slick-prev"><img src="Theme01/Assets/css/images/prev.png" alt="prev" /></button>'
});

$(".product__slider").slick({
	nextArrow: '<button type="button" class="slick-next"><img src="Theme01/Assets/css/images/next.png" alt="next" /></button>',
	prevArrow: '<button type="button" class="slick-prev"><img src="Theme01/Assets/css/images/prev.png" alt="prev" /></button>',
	slidesToShow: 4,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 3
		}
	}, {
		breakpoint: 600,
		settings: {
			slidesToShow: 1
		}
	}]
});